const fs = require("fs-extra");
const path = require("path");
const chalk = require("chalk");

const chokidar = require("chokidar");
const globby = require("globby");

const rollup = require("rollup");
const rollupTypescript = require("@rollup/plugin-typescript");
const rollupResolve = require("@rollup/plugin-node-resolve").nodeResolve;
const rollupSvelte = require("rollup-plugin-svelte");
const rollupAlias = require("@rollup/plugin-alias");
const sveltePreprocess = require("svelte-preprocess");

const buildTools = require("build-tools");

function timeUnit(time) {
  return time < 500
    ? `${Math.round(time * 10) / 10}ms`
    : `${Math.round(time / 10) / 100}s`;
}

async function time(name, fn, ...args) {
  const start = performance.now();
  await fn();
  const time = performance.now() - start;

  console.log(chalk.blue(name), "completed in", chalk.green(timeUnit(time)));
}

// Configuration
const options = {
  outDir: path.resolve("dist"),
  manifest: "./src/module.json",
  tsEntrypoint: "./src/quick-insert.ts",
};
// Patterns for watch & compile
const sourceGroups = {
  // Folders are copied as-is
  folders: ["templates", "lang"],
  // Files are copied following pattern
  statics: ["src/**/*.css"],
};

const packageTool = new buildTools.PackageTool(options);

/********************/
/*		BUILD		*/
/********************/

/**
 * Build TypeScript
 */
const rollupOptions = {
  input: options.tsEntrypoint,
  preserveEntrySignatures: "allow-extension",
  plugins: [
    rollupAlias({
      entries: [
        {
          find: "module",
          replacement: path.resolve("./src/module"),
        },
      ],
    }),
    rollupSvelte({
      emitCss: false,
      preprocess: sveltePreprocess(),
    }),
    rollupTypescript(),
    rollupResolve(),
  ],
  manualChunks(id) {
    if (id.includes("node_modules")) {
      return "vendor";
    }
  },
};

const rollupOutputOptions = {
  dir: "./dist",
  format: "es",
  preferConst: true,
  minifyInternalExports: false,
  exports: "auto",
  chunkFileNames: "[name].js",
  sourcemap: true,
};

async function buildTS() {
  const bundle = await rollup.rollup(rollupOptions);
  await bundle.write(rollupOutputOptions);
}

async function watchTS() {
  const watchOptions = { ...rollupOptions, output: rollupOutputOptions };
  const watcher = rollup.watch(watchOptions);

  watcher.on("event", (event) => {
    if (event.code === "BUNDLE_START") {
      console.log(chalk.blue("TypeScript"), "started");
    }
    if (event.code === "ERROR") {
      const error = event.error;
      console.warn(
        chalk.blue("TypeScript"),
        "error:",
        `${error.plugin}:${error.pluginCode}`,
        "\n" + error.frame,
        `\n${error.filename}`
      );
    }
    if (event.code === "BUNDLE_END") {
      console.log(
        chalk.blue("TypeScript"),
        "built in",
        chalk.green(timeUnit(event.duration))
      );
    }
  });

  // This will make sure that bundles are properly closed after each run
  watcher.on("event", ({ result }) => {
    if (result) {
      result.close();
    }
  });
}

/**
 * Copy static files
 */
async function copyFolders() {
  for (const folder of sourceGroups.folders) {
    if (fs.existsSync(folder)) {
      await fs.copy(folder, path.join(options.outDir, folder));
    }
  }
}

async function copyStatics() {
  const paths = await globby(sourceGroups.statics);
  await Promise.all(
    paths.map((file) => {
      const out = path.join(options.outDir, path.basename(file));
      return fs.copyFile(file, out);
    })
  );
}

/**
 * Watch for changes for each build step
 */
function buildWatch() {
  chokidar
    .watch(options.manifest)
    .on("all", () => time("Manifest", packageTool.buildManifest));

  chokidar.watch(sourceGroups.folders).on("all", (evt, file) => {
    time(`Copy Folders - ${file}`, async () => {
      if (evt === "addDir") {
        return fs.ensureDir(path.join(options.outDir, file));
      }

      if (evt === "add" || evt === "change") {
        const out = path.join(options.outDir, file);
        return fs.copyFile(file, out);
      }

      if (evt === "unlink") {
        const out = path.join(options.outDir, file);
        return fs.unlink(out);
      }
    });
  });

  chokidar
    .watch(sourceGroups.statics)
    .on("all", () => time("Copy Statics", copyStatics));

  watchTS();
}

async function build() {
  await time("Manifest", packageTool.buildManifest);
  await time("TS", buildTS);
  await time("Copy folders", copyFolders);
  await time("Copy statics", copyStatics);
}

async function clean() {
  if (!fs.existsSync(options.outDir)) {
    return;
  }

  const files = await fs.readdir(options.outDir);
  console.log(" ", chalk.yellow("Files to clean:"));
  console.log("   ", chalk.blueBright(files.join("\n    ")));

  await Promise.all(
    files.map((filePath) => fs.remove(path.join(options.outDir, filePath)))
  );
}

// Single tasks

const commands = {
  build: () => build(),
  watch: () => buildWatch(),
  clean: () => clean(),
  package: () => packageTool.package(),
  link: () => buildTools.linkUserData(options.manifest, options.outDir),
  unlink: () => buildTools.unlinkUserData(options.manifest),
};

const cmds = process.argv.slice(2);

(async () => {
  for (const cmd of cmds) {
    if (!commands[cmd]) {
      console.error(`Unknown command "${cmd}"`);
      process.exit(1);
    }
    if (cmds.length > 1) {
      console.log(chalk.magenta(cmd));
    }
    await commands[cmd]();
  }
})();
